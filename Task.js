// const pictureChange = document.querySelector('.images-wrapper')
const pictureChange = document.querySelectorAll('.image-to-show')
const buttonStop = document.querySelector('.btn')
const buttonResume = document.querySelector('.btn')


function pictureReplacement(image, duration, indexElement){
    image.forEach(el => el.style.display = 'none')
    let timer = setInterval(() => {
        image.forEach(el => el.style.display = 'none')
        const item = image[indexElement]
        item.style.display = ''
        console.log(item)
        indexElement++
        if (indexElement >= image.length) {
            clearInterval(timer)
        }    
    }, duration);
}

pictureReplacement(pictureChange, 3000, 0)

buttonStop.addEventListener('click', function(){
    clearInterval(timer)
    buttonResume.disabled = false;
    buttonStop.disabled = true
})


buttonResume.addEventListener('click', function(){
    timer = setInterval(pictureReplacement, 3000)
    buttonResume.disabled = false;
    buttonStop.disabled = true
})









